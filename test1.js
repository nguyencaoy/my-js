function isprime(n){
    //flag = 0 => không phải số nguyên tố
    //flag = 1 => số nguyên tố
    
    let flag = 1;

    if (n <2) return flag = 0; /*Số nhỏ hơn 2 không phải số nguyên tố => trả về 0*/
    
    /*Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác không*/
    let i = 2;
    let j = 2;
    while(i <n){
        if( n%i===0 ) {
            flag = 0;
            break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
        }
        i++;
    }
    while(j <n){
        if( n%j===0 ) {
            flag = 0;
            break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
        }
        j++;
    }
    return flag;
}


const arr = new Array(100);

function fill(n) {
    if (n === arr.length)
        return;
    else {
        arr[n] = n + 1;
        fill(n + 1);
    }
}
fill(0);

const result1 = [];
const result2 = [];
const result = [];
/*Tìm  các cặp số nguyên tố trong mảng*/
for (let i = 0; i < arr.length; i++){

  for(let j = 0; j < i; j++){
    if (isprime(arr[j]) === 1)  result2.push( arr[j]);
  }
    if (isprime(arr[i]) === 1)  result1.push( arr[i]);
  result.push(result1,result2);
}

console.log(result)
